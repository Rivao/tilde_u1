﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace Uzdevums1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the equation in the form of n1 nj ... n(j+1) = n");
            var inputString = Console.ReadLine();
            var inputStringArray = inputString.Split(" ");
            var numberArray = new List<int>();
            var tempNumber = 0;
            foreach (var num in inputStringArray)
            {
                if(int.TryParse(num, out tempNumber))
                {
                    numberArray.Add(tempNumber);
                }
            }
            var operators = Enumerable.Repeat('+', numberArray.Count - 1).ToList();
            var isAnswerFound = false;
            var i = 0;
            var minusPositions = new List<int>();
            // respresents where is the fixed moving minus operator
            // contains positive value only if there is a fixed moving minus operator being used
            var fixedMovingOperatorIndex = -1;
            // Last number will always be the sum entered by the user
            var result = numberArray[numberArray.Count - 1];
            while (!isAnswerFound)
            {
                var sum = 0;
                // start by getting the max value       
                for (var n = 0; n < numberArray.Count - 1; n++)
                {
                    if (operators[n] == '+') sum += numberArray[n];
                    else sum -= numberArray[n];

                }
                if (sum == result)
                {
                    isAnswerFound = true;
                }
                else
                {
                // reset previous operator to + unless this is the first moving minus operator
                // and unless previous operator is the fixed moving operator
                if (i > 0 && fixedMovingOperatorIndex != i - 1)
                {
                    operators[i - 1] = '+';
                }
                // when index is the same as count, last operator in the sequence has already been checked
                if (i < operators.Count)
                {
                    operators[i] = '-';
                }
                    // all of possibilities explored with current number of minus signs
                    // adding more
                    if (i == operators.Count)
                    {
                        if (minusPositions.Count > 0)
                        {
                            // when furthest fixed minus operator reaches the end, add new one
                            if (minusPositions[minusPositions.Count - 1] == operators.Count - 1)
                            {
                                // reset fixed minus operators that moves around
                                for (var j = 0; j < minusPositions.Count; j++)
                                {
                                    minusPositions[j] = j;
                                }
                                minusPositions.Add(minusPositions.Count);
                            }
                            else if (minusPositions[minusPositions.Count - 1] > operators.Count - 1)
                            {
                                break;
                            }
                            else
                            {
                                // move fixed minus operators to the right
                                for (var j = 0; j < minusPositions.Count; j++)
                                {
                                    minusPositions[j]++;
                                }
                            }
                            operators = Enumerable.Repeat('+', numberArray.Count - 1).ToList();
                            // set fixed minus operators
                            for (var j = 0; j < minusPositions.Count; j++)
                            {
                                operators[minusPositions[j]] = '-';
                            }
                            // one after furthest minus position
                            i = minusPositions[minusPositions.Count - 1] + 1;
                            fixedMovingOperatorIndex = minusPositions[minusPositions.Count - 1];

                        }
                        else
                        {
                            minusPositions.Add(0);
                            i = 1;
                            fixedMovingOperatorIndex = 0;
                            operators[0] = '-';
                        }
                        continue;
                    }
                    else
                    {
                        i++;
                        continue;
                    }
                }
            }
            var answer = "";
            if (isAnswerFound)
            {
                for (i = 0; i < numberArray.Count; i++)
                {
                    // is equal sign reached
                    if (i == numberArray.Count - 1)
                    {
                        answer += $"= {numberArray[i]}";
                    }
                    // no need to add the plus sign at the beginning
                    else if (i == 0 && operators[i] == '+')
                    {
                        answer += $"{numberArray[i]} ";
                    }
                    else
                    {
                        answer += $"{operators[i]} {numberArray[i]} ";
                    }
                }
            }
            else
            {
                answer = "Equation has no solution.";
            }
            Console.WriteLine(answer);
        }


    }
}
